var petsServices = angular.module('petsServices', ['ngResource']);
var headers = { };

petsServices.factory('HelperService', ['$routeParams', '$location', function($routeParams, $location){
	return {
		goTo: function(url){
			$location.url(url);
		},
		showError: function (text) {
            var div = $('div.error');
            $(div).show('400', function() {
				$(this).text(text);
			});
            $(div).click(function(event) {
				$('div.error').hide();
			});
		}
	}
}])
.factory('FileService', ['BACKEND_URL', '$window', function(BACKEND_URL, $window){
	return {
		upload: function(token){
			$("input[name='token']").val(token);
			var options = {
				url: BACKEND_URL + '/api/v1/user/photo',
				type: 'put',
				success: function(response){
					$window.location.reload();
				}
			};

			$('#userChangePhoto').submit(function() {
				$(this).ajaxSubmit(options,{
					error: function(xhr) {
						status('Error: ' + xhr.status);
					}
				});
				return false;
			});
		}
	}
}])
.factory('User', ['$http', '$window', '$cookies', 'HelperService', 'BACKEND_URL', function($http, $window, $cookies, HelperService, BACKEND_URL){
		return {
		reg: function(userEmail, userPassword) {
			var req = {
				method: 'POST',
				url: BACKEND_URL +'/api/v1/user/register/',
				headers: '',
				data: { 
					email: userEmail, 
					password: userPassword 
				}
            };
            $http(req).success(function(message){
                console.log(message);
			}).error(function(error){

			});
		},
        login: function(userEmail, userPassword){
            var req = {
                method: 'POST',
                url: BACKEND_URL + '/api/v1/user/login/',
                headers: '',
                data: {
                    email: userEmail,
                    password: userPassword
                }
            };
            $http(req).success(function(message){
				var token = message.token;
				$http.defaults.headers.common['X-Token'] = token;
				$cookies.accessToken = token;
				HelperService.goTo('/timeline');
            }).error(function(error){
				console.log(error);
            });
        },
		logout: function() {
			$cookies.accessToken = '';
			HelperService.goTo('/login');
		}
	}
}])
.factory('UserInfo', function($resource,BACKEND_URL){
		return $resource(BACKEND_URL + '/api/v1/:user', {}, {
			info:{ method: 'GET', params: { user: 'user', token: 'token' }},
			update:{ method: 'PUT', params: { user: 'user' } }
		});
	});