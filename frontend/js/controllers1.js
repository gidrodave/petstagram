/* Controllers */

var petsApp = angular.module('instaPetsControllers', []); 

petsApp.controller('TimelineListCtrl', ['$scope', '$routeParams', '$location', 'HelperService', function($scope, $routeParams, $location, HelperService){

	var currentUser = Parse.User.current();

	if (currentUser) {
		$scope.pets = [
			{
				"photo" : "dist/images/1.jpg",
				"date" : "Jan 12"
			},
			{
				"photo" : "dist/images/2.jpg",
				"date" : "Jan 13"
			},
			{
				"photo" : "dist/images/3.jpg",
				"date" : "Jan 14"
			},
			{
				"photo" : "dist/images/5.jpg",
				"date" : "Jan 15"
			},
			{
				"photo" : "dist/images/6.jpg",
				"date" : "Jan 16"
			}
		];
	} else {
		HelperService.goTo('/login');
	}
}]);

petsApp.controller('RegistrationController', ['$scope', '$routeParams', '$location', 'HelperService', function($scope, $routeParams, $location, HelperService) {
	// Login register tabs
	$('li.login').on('click', function(event) {
		$('.tabs-content.login').addClass('active');
		$('.tabs-content.register').removeClass('active');
	});
	$('li.register').on('click', function(event) {
		$('.tabs-content.login').removeClass('active');
		$('.tabs-content.register').addClass('active');
	});
	$('.bl-form-background').css('height', $('.tabs').css('height'));
	
	var iframe = $('#vimeo_player')[0];
    var player = $f(iframe);
    var status = $('.status');

    player.addEvent('ready', function() {
        player.api('setVolume', 0);
    });

    $scope.master = {};

    var userInfo;
    var currentUser = Parse.User.current();

    if (currentUser) {
		HelperService.goTo('/timeline');
	}

    $scope.register = function(user) {
        $scope.master = angular.copy(user);

        var userInfo = $scope.master;

        if(userInfo.password == userInfo.repassword){
        	$('div.error').hide();

	        // Register user
	        var user = new Parse.User();
	        user.set("username", userInfo.email);
	        user.set("password", userInfo.password);
	        user.set("email", userInfo.email);
	        user.set("emailVerified");

	        user.signUp(null, {
			  success: function(user) {
			    
			  },
			  error: function(user, error) {
			    // Show the error message somewhere and let the user try again.
			    HelperService.showError(error.message);
			  }
			});
    	} else {
    		HelperService.showError('Passwords do not match');
    	}
    };

    $scope.login = function(user) {
        $scope.master = angular.copy(user);
        var userInfo = $scope.master;
        
        Parse.User.logIn(userInfo.email, userInfo.password, {
		  success: function(s_user) {
		  	HelperService.goTo('/timeline');
		  },
		  error: function(s_user, error) {
		  	HelperService.showError(error.message);
		  }
		});
    };
}]);

petsApp.controller('EditCtrl', ['$scope', '$routeParams', '$location', 'HelperService', 'FileService', function($scope, $routeParams, $location, HelperService, FileService) {
	var currentUser = Parse.User.current();
	
	if (currentUser) {
		$scope.uploadFileFunc = function() { 
			FileService.upload();
		}
	} else {
		HelperService.goTo('/login');
	}	
}]);

petsApp.controller('HeaderCtrl', ['$scope', '$routeParams', '$location', 'HelperService', function($scope, $routeParams, $location, HelperService) {
	var currentUser = Parse.User.current();
	
	if (currentUser) {
		$scope.logout = function() { 
			Parse.User.logOut();
			HelperService.goTo('/login');
		}
		$scope.edit = function() { 
			HelperService.goTo('/edit');
		}
	} else {
		HelperService.goTo('/login');
	}
}]);