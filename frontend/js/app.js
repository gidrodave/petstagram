'use strict';

var petsApp = angular.module('petsApp', [
	'ngRoute',
	'ngResource',
	'instaPetsControllers',
	'petsServices',
	'ngImgCrop',
	'angular-datepicker',
	'ngCookies'
]);
petsApp.constant('BACKEND_URL', 'http://127.0.0.1:3001');
petsApp.config(['$routeProvider','$resourceProvider',
	function($routeProvider, $resourceProvider) {
		$resourceProvider.defaults.stripTrailingSlashes = false;
		$routeProvider.
			when('/login', {
				templateUrl: 'view/login.html',
				controller: 'RegistrationController'
			}).
			when('/timeline', {
				templateUrl: 'view/timeline.html',
				controller: 'TimelineListCtrl'
			}).
			when('/edit', {
				templateUrl: 'view/edit.html',
				controller: 'EditCtrl'
			}).
			otherwise({
			redirectTo: '/login'
	});
}]);