/* Controllers */

var petsApp = angular.module('instaPetsControllers', []); 

petsApp.controller('TimelineListCtrl', ['$scope', '$routeParams', '$location', '$window', '$cookies', 'HelperService',
	function($scope, $routeParams, $location, $window, $cookies, HelperService){
		if ($cookies.accessToken) {
			$scope.pets = [
				{
					"photo" : "img/1.jpg",
					"date" : "Jan 12"
				},
				{
					"photo" : "img/2.jpg",
					"date" : "Jan 13"
				},
				{
					"photo" : "img/3.jpg",
					"date" : "Jan 14"
				},
				{
					"photo" : "img/5.jpg",
					"date" : "Jan 15"
				},
				{
					"photo" : "img/6.jpg",
					"date" : "Jan 16"
				}
			];
		} else {
			HelperService.goTo('/login');
		}
}]);

petsApp.controller('RegistrationController', ['$scope', '$routeParams', '$location', '$window', 'HelperService', 'User', '$cookies',
	function($scope, $routeParams, $location, $window, HelperService, User, $cookies) {
		if($cookies.accessToken){
			HelperService.goTo('/timeline');
		} else {
			// Login register tabs
			$('li.login').on('click', function(event) {
				$('.tabs-content.login').addClass('active');
				$('.tabs-content.register').removeClass('active');
			});
			$('li.register').on('click', function(event) {
				$('.tabs-content.login').removeClass('active');
				$('.tabs-content.register').addClass('active');
			});
			$('.bl-form-background').css('height', $('.tabs').css('height'));

			var iframe = $('#vimeo_player')[0];
			var player = $f(iframe);

			player.addEvent('ready', function() {
				player.api('setVolume', 0);
			});

			$scope.master = {};

			$scope.register = function(user) {
				$scope.master = angular.copy(user);

				var userInfo = $scope.master;
				User.reg(userInfo.email, userInfo.password);
			};

			$scope.login = function(user) {
				$scope.master = angular.copy(user);
				var userInfo = $scope.master;

				User.login(userInfo.email, userInfo.password);
			};
		}
	}]);

petsApp.controller('EditCtrl', ['$scope', '$routeParams', '$location', '$window', '$cookies', 'HelperService', 'FileService', 'User', 'UserInfo', 'BACKEND_URL',
	function($scope, $routeParams, $location, $window, $cookies, HelperService, FileService, User, UserInfo, BACKEND_URL) {
		$('.btn-edit-user-photo').on('click', function(){
			$('.bl-edit-photo').toggle();
		});
		$('.btn-choose-file').on('click', function(){
			var input = $("#inpt-choose-file");
			input.replaceWith(input.clone(true));
			input.click();
		});
		$scope.options = {
			format: 'yyyy-mm-dd',
			onClose: function(e) {
				var btn = $('#btnSave');
				btn.focus();
				btn.blur();
			}
		};
		var token = $cookies.accessToken;
		if (token) {
			$scope.user_info = UserInfo.info({token:token});


			FileService.upload(token);
			$scope.myImage='';
			$scope.myCroppedImage= '';

			var handleFileSelect=function(evt) {
				var file=evt.currentTarget.files[0];
				var reader = new FileReader();
				reader.onload = function (evt) {
					$scope.$apply(function($scope){
						$scope.myImage=evt.target.result;
					});
				};
				reader.readAsDataURL(file);
			};
			angular.element(document.querySelector('#inpt-choose-file')).on('change',handleFileSelect);

			$scope.send_update_user = {};

			$scope.save = function(user_info){
				$scope.send_update_user = angular.copy(user_info);
				var user_data = {
					"token": token,
					"user": angular.toJson($scope.send_update_user)
				};
				UserInfo.update(user_data);
			};
		} else {
			HelperService.goTo('/login');
		}
}]);

petsApp.controller('HeaderCtrl', ['$scope', '$routeParams', '$location', '$window', '$cookies', 'HelperService', 'User', 'UserInfo',
	function($scope, $routeParams, $location, $window, $cookies, HelperService, User, UserInfo) {
		if ($cookies.accessToken) {
			$scope.user_info = UserInfo.info({token: $cookies.accessToken});
			$scope.logout = function() {
				User.logout();
			};
			$scope.edit = function() {
				HelperService.goTo('/edit');
			};
			$scope.timeline = function(){
				HelperService.goTo('/timeline');
			};
		} else {
			HelperService.goTo('/login');
		}
}]);