# Petstagram #

## Technology: ##
1. NodeJS
2. MongoDB
3. AngularJS

## Run project ##

## First step ##
______________
### Create db ###
Run node file in backend folder
```
#!command
node createDB.js

```

## Second step ##
______________
### backend ###
First install node_modules
In backend folder run command:
```
#!command
npm i

```


After install node_modules run backend server:
```
#!command
node app.js

```

## Third step ##
______________
### fronend ###
In frontend folder run server for fronend:
```
#!command
node app.js

```