var express = require('express'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    mongoose = require('./libs/mongoose'),
    http = require('http'),
    path = require('path'),
    config = require('./config'),
    log = require('./libs/log')(module);

var app = express();

app.use(bodyParser({limit: '10mb'}));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());

app.use('/files',express.static(__dirname + '/files'));

app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization, Origin, Accept');
    next();
});

require('./routes')(app);

app.use(function(err,req,res,next){
    log.error(err);
});


app.listen(config.get("port"), function(){
    log.info('Express server listening on port ' + config.get('port'));
});