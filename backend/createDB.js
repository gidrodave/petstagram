var mongoose = require('libs/mongoose');
var async = require('async');

async.series([
    open,
    dropDB,
    requireModels,
    createDB
],function(err){
    console.log(arguments);
    mongoose.disconnect();
});

function open(callback){
    mongoose.connection.on('open',callback);
}

function dropDB(callback){
    var db = mongoose.connection.db;
    db.dropDatabase(callback);
}

function requireModels(callback){
    require('models/user');

    async.each(Object.keys(mongoose.models),function(modelName,callback){
        mongoose.models[modelName].ensureIndexes(callback);
    },callback);
}

function createDB(callback){
    var users = [
        {email:'sannyya35@gmail.com',password: '123123', token: "1sae1eqsa325sfsf"},
        {email:'sannyya35@ya.ru',password: '321321', token: "1sae1eqsa325sfsf"},
        {email:'sannyya35@mail.ru',password: '111222', token: "1sae1eqsa325sfsf"}
    ];

    async.each(users, function(userData,callback){
        var user = new mongoose.models.User(userData);
        user.save(callback);
    },callback);
}