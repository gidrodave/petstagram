/**
 * Include modules
 */
var crypto = require('crypto'),
    async = require('async'),
    mongoose = require('../libs/mongoose'),
    jwt = require("jsonwebtoken"),
    Schema = mongoose.Schema;

/**
 * Create new user schema
 */
var schema = new Schema({
    email: {
        type: String,
        unique: true,
        required: true
    },
    hashedPassword: {
        type: String,
        required: true
    },
    salt: {
        type: String,
        required: true
    },
    token: {
        type: String,
        unique: true
    },
    created: {
        type: Date,
        default: Date.now
    },
    petPhoto: {
        type: String,
        default: null
    },
    petName: {
        type: String,
        default: null
    },
    petBirthday: {
        type: String,
        default: null
    }
});

/**
 * Encrypt password
 * @param password
 * @returns {*}
 */
schema.methods.encryptPassword = function(password){
  return crypto.createHmac('sha', this.salt).update(password).digest('hex');
};

schema.virtual('password')
    .set(function(password){
        this.__plainPassword = password;
        this.salt = Math.random() + '';
        this.hashedPassword = this.encryptPassword(password)
    })
    .get(function(){ return this.__plainPassword; });

/**
 * Check password
 * @param password
 * @returns {boolean}
 */
schema.methods.checkPassword = function(password){
    return this.encryptPassword(password) === this.hashedPassword;
};

/**
 * Registration new user function
 * @param email
 * @param password
 * @param callback
 */
schema.statics.registration = function(email,password, callback){
    var User = this;

    async.waterfall([
        function(callback) {
            User.findOne({email: email}, callback);
        },
        function(user, callback) {
            if (user) {
                callback("User already exists!");
            } else {
                var userModel = new User();
                userModel.email = email;
                userModel.password = password;
                var userObj = {
                    email: email,
                    password: password
                };
                userModel.token = jwt.sign(userObj, 'y2TtdFwT4KT89RxASmqk');
                userModel.save(function(err, user){
                    if (err) return callback(err);
                    callback(user);
                });
            }
        }
    ], callback);
};

/**
 * Authenticate new user function
 * @param email
 * @param password
 * @param callback
 */
schema.statics.authenticate = function(email,password, callback){
    var User = this;

    async.waterfall([
        function(callback) {
            User.findOne({email: email}, callback);
        },
        function(user, callback) {
            if (user) {
                if(user.checkPassword(password)){
                    var res_user = {
                        id: user._id,
                        token: user.token
                    };
                    callback(null, res_user);
                } else {
                    callback("Incorrect email/password");
                }
            } else {
                callback("User not found");
            }
        }
    ], callback);
};

/**
 * Get user information
 * @param token
 * @param callback
 */
schema.statics.getUserInfo = function(token, callback){
    var User = this;

    async.waterfall([
        function(callback){
            User.findOne({token: token}, callback);
        },
        function(user, callback) {
            if(user) {
                callback(null ,user);
            } else {
                callback("User not found");
            }
        }
    ], callback);
};

schema.statics.changePhoto = function(token, photo_path, callback){
    var User = this;

    async.waterfall([
       function(callback){
           User.findOne({token: token}, function(err, user){
               if(err) throw err;
               user.petPhoto = photo_path;
               user.save(function(err){
                   if(err) {
                       callback(err, null);
                   } else {
                       callback(null, "Photo updated");
                   }
               });
           });
       }
    ],callback);
};

schema.statics.updateUserInfo = function(user_data, callback ){
    var data = JSON.parse(user_data);
    var User = this;

    async.waterfall([
        function(callback){
            User.findOne({_id: data.id}, function(err, user){
                if(err) throw err;
                user.email = data.email;
                user.petPhoto = data.pet_photo;
                user.petName = data.pet_name;
                user.petBirthday = data.pet_birthday;
                user.save(function(err, data){
                    if(err) {
                        callback(err, null);
                    } else {
                        callback(null, "User updated! Successfully!");
                    }
                });
            });
        }
    ],callback);

};

exports.User = mongoose.model('User', schema);