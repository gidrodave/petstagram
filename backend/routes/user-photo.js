var User = require('../models/user').User,
    fs = require('fs'),
    path = require('path'),
    async = require('async');

exports.put = function(req, res){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With,X-Token");

    User.getUserInfo(req.body.token, function (err, user){
        if (err) throw err;
        var user_info = {
            id: user._id
        };
        var homePATH = path.dirname(require.main.filename);
        var user_path = homePATH + path.normalize('/files/') + user_info.id;
        var toPath = user_path + '/' + 'user_photo.jpg';
        var data = req.body.userPhoto.replace(/^data:image\/png;base64,/, "");

        async.waterfall([
            function(callback){
                fs.exists(user_path, function(exists){
                    if(exists){
                        callback(null);
                    } else {
                        fs.mkdir(user_path, function(err, result){
                            if(err) throw err;
                            callback(null, result);
                        });
                    }
                });
            },
            function(calback){
                fs.writeFile(toPath, data, 'base64', function(err){
                    if(err) throw err;
                    var responsPath = path.normalize('/files/') + user_info.id + '/user_photo.jpg';
                    var normalizePath = path.normalize(responsPath);
                    calback(null, normalizePath);
                });
            }
        ], function(err,result){
            if(err) throw err;
            var path = 'http://' + req.get('host') + result.replace(/\\/g, '/');
            User.changePhoto(req.body.token, path, function(err, result){
                if(err){
                    throw err;
                } else {
                    res.json(result);
                }
            });
        });
    });
};