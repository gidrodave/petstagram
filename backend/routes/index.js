var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

module.exports = function(app){
    app.post('/api/v1/user/register/', require('./register').post);
    app.post('/api/v1/user/login/', require('./login').post);
    app.get('/api/v1/user/', require('./user').get);
    app.put('/api/v1/user/', require('./user').put);
    app.put('/api/v1/user/photo', multipartMiddleware, require('./user-photo').put);
};