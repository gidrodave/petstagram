var User = require('../models/user').User;

exports.post = function(req, res){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    var email = req.body.email;
    var password = req.body.password;
    User.registration(email, password, function (err, user){
        if (err) throw err;
        res.json(user);
    });
};