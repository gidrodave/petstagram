var User = require('../models/user').User,
    fs = require('fs'),
    path = require('path'),
    async = require('async');

exports.get = function(req, res){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With,X-Token");
    User.getUserInfo(req.query.token, function (err, user){
        if (err) throw err;
        res.send({
            id: user._id,
            email: user.email,
            pet_photo: user.petPhoto,
            pet_name: user.petName,
            pet_birthday: user.petBirthday
        });
    });
};

exports.put = function(req, res){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    User.getUserInfo(req.body.token, function (err, user){
        if (err) throw err;
        User.updateUserInfo(req.body.user, function(err, user){
            if (err) throw err;
            res.send(user);
        });
    });
};